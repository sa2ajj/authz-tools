#
"""
Basic interface to .htpasswd files
"""

import os
from random import randint
from crypt import crypt
from string import lowercase, uppercase, digits

_SYMBOLS = './' + lowercase + uppercase + digits

class HTPasswdFile:
    """
    The main interface class
    """
    def __init__ (self, fname):
        self.__fname = fname
        self.__index = {}
        self.__users = []

    def read(self):
        """
        Read information from the corresponding file
        """
        self.__index = {}
        self.__users = []

        with open(self.__fname, 'r') as data:
            for line in data.readlines():
                line = line.strip()

                parts = line.split(':')

                self.__index[parts[0]] = len(self.__users)
                self.__users.append(parts)

    def write(self):
        """
        Save the current state to the corresponding file
        """
        outname = self.__fname + '.new'

        with open(outname, 'w') as data:
            for record in self.__users:
                print >> data, ':'.join(record)

        os.unlink(self.__fname)
        os.rename(outname, self.__fname)

    def users(self):
        """
        Known users
        """
        return self.__index.keys()

    def add(self, user, password):
        """
        Add a new user with the given password
        """
        if user not in self.__index:
            self.__index[user] = len(self.__users)
            self.users.append ([user, ''])

            self.set(user, password)

    def check(self, user, password):
        """
        Check if the given user has the given password
        """
        if user in self.__index:
            crypted = self.__users[self.__index[user]][1]

            result = crypt(password, crypted) == crypted
        else:
            result = False

        return result

    def set(self, user, password):
        """
        Set the password for the given user
        """
        if user in self.__index:
            salt = randint(0, 4095)

            first, second = salt / 64, salt % 64

            crypted = crypt(password, _SYMBOLS[first] + _SYMBOLS[second])

            self.__users[self.__index[user]][1] = crypted
        else:
            pass  # simply ignore!

    def disable(self, user_):
        """
        Disable the given user
        """
        raise RuntimeError('Not Implemented')

    def enable(self, user_):
        """
        Enable the given user
        """
        raise RuntimeError('Not Implemented')
