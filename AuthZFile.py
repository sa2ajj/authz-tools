#! /usr/bin/python

__copyright__ = 'Copyright (C) 2005, Mikhail Sobolev <mss@mawhrin.net>'

import sys, os
import re
import types

def format_header(what):
  if what == 'groups':
    r = '[groups]'
  elif what == 'admin':
    r = '[admin]'
  elif what[0] is None:
    r = '[%s]' % what[1]
  else:
    r = '[%s:%s]' % what

  return r

_header = re.compile(r'^\[((?P<groups>groups)|(?P<admin>admin)|((?P<repo>[^:]+):)?(?P<path>/.*))\]$')

class AuthZFile:
  def __init__(self, fname):
    self.fname = fname
    self.admin = None
    self.__admin_members = None
    self.groups = None
    self.__group_members = None
    self.sections = {}

  def read(self, fp = None):
    if fp is not None:
      f = fp
    else:
      f = open(self.fname, 'r')

    current = None
    lines = []

    for line in f.readlines():
      line = line.strip()

      m = _header.match(line)

      if m:
        if current is not None:
          section = '\n'.join(lines)

          if current == 'groups':
            self.groups = section
          elif current == 'admin':
            self.admin = section
          else:
            self.sections[current] = section

        if m.group('groups') is not None:
          current = 'groups'
        elif m.group('admin') is not None:
          current = 'admin'
        else:
          current = m.group('repo'), m.group('path')

        lines = []
      else:
        if current is not None:
          lines.append(line)

    if current is not None:
      section = '\n'.join(lines)

      if current == 'groups':
        self.groups = section
      elif current == 'admin':
        self.admin = section
      else:
        self.sections[current] = section

    if fp is None:
      f.close()

  def write(self, name = None):
    if name is None:
      outname = self.fname+'.new'
    else:
      outname = name

    f = open(outname,'w')

    if self.admin  is not None:
      print >> f, '[admin]'
      print >> f, self.admin

    if self.groups is not None:
      print >> f, '[groups]'
      print >> f, self.groups

    keys = self.sections.keys()
    keys.sort()

    for key in keys:
      print >> f, format_header(key)
      print >> f, self.sections[key]

    f.close()

    if name is None:
      os.unlink(self.fname)
      os.rename(outname, self.fname)

  def keys(self):
    return self.sections.keys()

  def __contains__(self, key):
    if key == 'groups':
      r = self.groups is not None
    elif key == 'admin':
      r = self.admin is not None
    else:
      r = key in self.sections

    return r

  def __getitem__(self, key):
    if key == 'groups':
      result = self.groups
    elif key == 'admin':
      result = self.admin
    else:
      assert type(key) == types.TupleType and len(key) == 2, 'Key must be a tuple that specifies repository and path'

      result = self.sections.get(key, None)

    return result

  def __setitem__(self, key, value):
    assert type(value) == types.StringType, 'The value must be a string'

    if key == 'groups':
      self.groups = value
      self.__group_members = None
    elif key == 'admin':
      self.admin = value
      self.__admin_members = None
    else:
      assert type(key) == types.TupleType and len(key) == 2, 'Key must be a tuple that specifies repository and path'

      self.sections[key] = value

  def __delitem__(self, key):
    if key == 'groups':
      self.groups = None
      self.__group_members = None
    elif key == 'admin':
      self.admin = None
      self.__admin_members = None
    else:
      assert type(key) == types.TupleType and len(key) == 2, 'Key must be a tuple that specifies repository and path'

      if key in self.sections:    # this version is more relaxed: we allow deletion of non-existant keys
        del self.sections[key]

  def repo_keys(self, repo):
    if repo == '':
      repo = None

    items = filter(lambda x, r = repo : x[0] == r, self.keys())

    def repopath(x):
      if ':' in x:
        return tuple(x.split(':', 1))
      else:
        return None, x

    items.sort(lambda x, y, p = repopath : cmp(p(x), p(y)))

    return items

  def member(self, user, group):
    if self.__group_members is None:
      tempo = {}

      for g in filter(lambda x : len(x) > 0 and x[0] != '#', map(lambda x : x.strip(),self.groups.split('\n'))):
        parts = g.split('=')
        name, members = parts[0].strip(), parts[1].strip()

        assert name not in tempo, 'duplicate definition of %s' % name

        tempo[name] = members.split(',')

      self.__group_members = tempo

    result = False        # not a member

    if group in self.__group_members:
      current = group

      checked = {}
      todo = [ current ]

      while todo:
        current = todo.pop(0)

        members = self.__group_members[current]

        if user in members:
          result = True
          break

        checked[current] = 1

        groups = map(lambda x : x[1:], filter(lambda x, c = checked : x[0] == '@' and not c.has_key(x[1:]), members))

        todo += groups

    return result

  def admin(self, user, group):
    if self.__admin_members is None:
      tempo = {}

      for g in filter(lambda x : len(x) > 0 and x[0] != '#', map(lambda x : x.strip(),self.admin.split('\n'))):
        parts = g.split('=')
        name, members = parts[0].strip(), parts[1].strip()

        assert name not in tempo, 'duplicate definition of %s' % name

        tempo[name] = members.split(',')

      self.__admin_members = tempo

    result = False        # not a member

    if group in self.__admin_members:
      current = group

      checked = {}
      todo = [ current ]

      while todo:
        current = todo.pop(0)

        members = self.__admin_members[current]

        if user in members:
          result = True
          break

        checked[current] = 1

        groups = map(lambda x : x[1:], filter(lambda x, c = checked : x[0] == '@' and not c.has_key(x[1:]), members))

        todo += groups

    return result
