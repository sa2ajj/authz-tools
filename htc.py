#! /usr/bin/python

"""
An internal helper to check that HTPFile.HTPasswdFile works as expected
"""

from getpass import getpass

from HTPFile import HTPasswdFile

def main():
    """
    Entry point
    """
    fname = 'some.passwd'

    hpf = HTPasswdFile(fname)
    hpf.read()

    user = raw_input('Enter UID: ').strip()
    pwd = getpass('Enter password: ')

    print 'checking:', hpf.check(user, pwd)

if __name__ == '__main__':
    main()
